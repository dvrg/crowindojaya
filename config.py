import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = os.environ.get("SECRET_KEY")

    CROWINDO_ADMIN = os.environ.get("CROWINDO_ADMIN")
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_RECORD_QUERIES = True

    RECAPTCHA_PUBLIC_KEY = os.environ.get("RECAPTCHA_PUBLIC_KEY")
    RECAPTCHA_PRIVATE_KEY = os.environ.get("RECAPTCHA_PRIVATE_KEY")

    # mail settings
    MAIL_DEFAULT_SUBJECT = "Dari Crowindojaya.com | "
    MAIL_DEFAULT_SENDER = os.environ.get("MAIL_DEFAULT_SENDER")
    MAIL_DEFAULT_RECIPIENTS = os.environ.get("MAIL_DEFAULT_RECIPIENTS")
    MAIL_SECONDARY = os.environ.get("MAIL_SECONDARY")
    MAIL_SERVER = os.environ.get("MAIL_SERVER")
    MAIL_PORT = 465
    MAIL_PORT_2 = 587
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    MAIL_USERNAME = os.environ.get("MAIL_USERNAME")
    MAIL_PASSWORD = os.environ.get("MAIL_PASSWORD")

    SSL_REDIRECT = False

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    DEVELOPMENT = True
    SQLALCHEMY_DATABASE_URI = os.environ.get("DEV_DATABASE_URL")


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL")

    @classmethod
    def init_app(cls, app):
        Config.init_app(app)

        import logging, socket
        from logging import FileHandler
        from logging.handlers import SMTPHandler

        socket.setdefaulttimeout(10)

        credentials = None
        secure = None
        if getattr(cls, "MAIL_USERNAME", None) is not None:
            credentials = (cls.MAIL_USERNAME, cls.MAIL_PASSWORD)
            if getattr(cls, "MAIL_USE_TLS", None):
                secure = ()
        mail_handler = SMTPHandler(
            mailhost=(cls.MAIL_SERVER, cls.MAIL_PORT_2),
            fromaddr=cls.MAIL_DEFAULT_SENDER,
            toaddrs=[cls.MAIL_DEFAULT_RECIPIENTS, cls.CROWINDO_ADMIN],
            subject=cls.MAIL_DEFAULT_SUBJECT + " Application Error!",
            credentials=credentials,
            secure=secure,
        )
        file_handler = FileHandler("flask-error.txt")
        mail_handler.setLevel(logging.ERROR)
        file_handler.setLevel(logging.ERROR)
        app.logger.addHandler(mail_handler)
        app.logger.addHandler(file_handler)


class HerokuConfig(ProductionConfig):
    @classmethod
    def init_app(cls, app):
        ProductionConfig.init_app(app)
        from werkzeug.contrib.fixers import ProxyFix

        app.wsgi_app = ProxyFix(app.wsgi_app)
        # log to stderr
        import logging
        from logging import StreamHandler

        file_handler = StreamHandler()
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)

    # SSL_REDIRECT = True if os.environ.get("DYNO") else False


class UnixConfig(ProductionConfig):
    # SSL_REDIRECT = True
    @classmethod
    def init_app(cls, app):
        ProductionConfig.init_app(app)

        import logging
        from logging.handlers import SysLogHandler

        syslog_handler = SysLogHandler()
        syslog_handler.setLevel(logging.WARNING)
        app.logger.addHandler(syslog_handler)


config = {
    "development": DevelopmentConfig,
    "production": ProductionConfig,
    "heroku": HerokuConfig,
    "unix": UnixConfig,
    "default": DevelopmentConfig,
}
