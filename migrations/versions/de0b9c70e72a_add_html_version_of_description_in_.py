"""add html version of description in Product

Revision ID: de0b9c70e72a
Revises: 87ea2c2b4489
Create Date: 2019-03-31 20:37:22.057638

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'de0b9c70e72a'
down_revision = '87ea2c2b4489'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('products', sa.Column('description_html', sa.Text(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('products', 'description_html')
    # ### end Alembic commands ###
