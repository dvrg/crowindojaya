from flask import Blueprint

admin = Blueprint(
    "admin", __name__, static_folder="static", template_folder="templates"
)

from . import views, errors
from ..models import Permissions


@admin.app_context_processor
def inject_permission():
    return dict(Permission=Permissions)
