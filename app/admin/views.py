import os, sys, uuid
from flask import url_for, render_template, request, redirect, flash, send_file
from werkzeug.utils import secure_filename
from slugify import slugify
from flask_login import login_required, login_user, logout_user, current_user
from flask_mail import Message
from io import BytesIO
from . import admin
from .. import db
from .forms import (
    CategoryForm,
    ProductForm,
    UserForm,
    ChangedPasswordForm,
    PasswordResetRequestForm,
)
from ..models import (
    Contact,
    Category,
    Product,
    User,
    Permissions,
    Role,
    category_count,
    product_count,
    contact_count,
    user_count,
)
from ..email import confirm_email
from datetime import datetime
from ..decorators import permission_required, admin_required
from sqlalchemy import or_

target = os.path.join(admin.static_folder, "uploads/photos")


def unique_name(data):
    file = data
    get_ext = file.filename.split(".")[-1]
    new_name = "%s.%s" % (uuid.uuid4().hex, get_ext)
    return new_name


@admin.route("/")
@login_required
def index():
    category = category_count()
    product = product_count()
    contact = contact_count()
    user = user_count()
    return render_template(
        "dashboard.html", cat=category, pro=product, con=contact, usr=user
    )


@admin.route("/profile/<username>")
@login_required
def profile(username):
    user = User()
    data = User.query.filter_by(username=username).first_or_404()
    return render_template("profile_detail.html", data=data, user=user)


@admin.route("/user/lihat", methods=["GET"])
@login_required
def view_user():
    data = (
        db.session.query(
            User.name,
            User.email,
            User.username,
            User.last_seen,
            User.confirmed,
            Role.name.label("role"),
        )
        .join(Role, Role.id == User.role_id)
        .all()
    )
    return render_template("view_user.html", data=data)


@admin.route("/user/tambah", methods=["GET", "POST"])
@login_required
def add_user():
    form = UserForm()
    if form.validate_on_submit():
        data = User(
            name=form.name.data,
            email=form.email.data,
            username=form.username.data,
            password=form.password.data,
        )
        db.session.add(data)
        db.session.commit()

        token = data.generate_confirmation_token()
        html = render_template("mail/confirm.html", data=data, token=token)
        confirm_email("Konfirmasi Akun", html, data.email)

        flash("Berhasil menambahkan user, cek email user.")
        return redirect(url_for("admin.view_user"))
    return render_template("user.html", form=form)


@admin.route("/user/edit/<string:username>", methods=["GET", "POST"])
@login_required
def edit_user(username):
    form = UserForm()
    data = User.query.filter_by(username=username).first_or_404()
    if form.validate_on_submit():
        data.name = form.name.data
        data.email = form.email.data
        data.username = form.username.data
        db.session.commit()
        flash("Data berhasil di update.")
        return redirect(url_for("admin.view_user"))
    form.name.data = data.name
    form.email.data = data.email
    form.username.data = data.username
    return render_template("user.html", form=form)


@admin.route("/user/hapus/<string:username>", methods=["GET", "POST"])
@login_required
def delete_user(username):
    data = User.query.filter_by(username=username).first_or_404()
    db.session.delete(data)
    db.session.commit()
    flash("Data berhasil dihapus.")
    return redirect(url_for("admin.view_user"))


@admin.route("/user/ganti-password", methods=["GET", "POST"])
@login_required
def change_password():
    form = ChangedPasswordForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.old_password.data):
            current_user.password = form.password.data
            db.session.add(current_user)
            db.session.commit()
            flash("Berhasil mengganti password. Silahkan login kembali")
            return redirect(url_for("auth.logout"))
        else:
            flash("Password salah.")
    return render_template("change_password.html", form=form)


@admin.route("/user/reset-password/<string:username>", methods=["GET", "POST"])
@login_required
def password_reset_request(username):
    user = User.query.filter_by(username=username).first_or_404()
    if user:
        token = user.generate_reset_token()
        html = render_template("mail/reset_password.html", user=user, token=token)
        confirm_email("Reset Password", html, user.email)
        flash("Intruksi reset password telah dikirimkan ke email.")
        return redirect(url_for("admin.view_user"))


@admin.route("/kontak/lihat", methods=["GET"])
@login_required
def view_contact():
    data = Contact.query.order_by(Contact.create_on.desc())
    return render_template("view_contact.html", data=data)


@admin.route("/gambar/<string:filename>", methods=["GET"])
def load_image_category(filename):
    # data = Product.query(Product.image_data.label('product_image'), Category.image_data.label('category_image')).join(Category, Product.category_id == Category.id).filter_by(or_(Product.image_filename == filename, Category.image_filename == filename)).first_or_404()
    data = Category.query.filter_by(image_filename=filename).first_or_404()
    # if data.product_image:
    #    print('Gambar Produk OK')
    # elif data.category_image:
    #    print('Gambar Category OK')
    # else:
    #    print('Empty')
    return send_file(
        BytesIO(data.image_data),
        mimetype="images/generic",
        as_attachment=True,
        attachment_filename=data.image_filename,
    )


@admin.route("/gambar/produk/<string:filename>", methods=["GET"])
def load_image_product(filename):
    data = Product.query.filter_by(image_filename=filename).first_or_404()
    return send_file(
        BytesIO(data.image_data),
        mimetype="images/generic",
        as_attachment=True,
        attachment_filename=data.image_filename,
    )


@admin.route("/kategori/tambah", methods=["GET", "POST"])
@login_required
def add_category():
    form = CategoryForm()
    if form.validate_on_submit():
        data = Category(
            image_data=form.photos.data.read(),
            name=form.name.data.lower(),
            description=form.description.data,
            image_filename=unique_name(form.photos.data),
            slug=slugify(form.name.data),
        )
        db.session.add(data)
        db.session.commit()
        flash("Data berhasil tersimpan.")
        return redirect(url_for("admin.view_category"))
    return render_template("category.html", form=form)


@admin.route("/kategori/lihat")
@login_required
def view_category():
    data = Category.query.all()
    return render_template("view_category.html", data=data)


@admin.route("/kategori/edit/<slug>", methods=["GET", "POST"])
@login_required
def edit_category(slug):
    form = CategoryForm()
    data = Category.query.filter_by(slug=slug).first_or_404()
    if form.validate_on_submit():
        data.slug = slugify(form.name.data)
        data.name = form.name.data
        data.description = form.description.data
        data.image_filename = unique_name(form.photos.data)
        data.image_data = form.photos.data.read()
        db.session.commit()
        flash("Data berhasil di update.")
        return redirect(url_for("admin.view_category"))
    form.name.data = data.name
    form.description.data = data.description
    # form.photos.data = data.image_filename
    return render_template("category.html", form=form)


@admin.route("/kategori/hapus/<slug>", methods=["GET", "POST"])
@login_required
def delete_category(slug):
    data = Category.query.filter_by(slug=slug).first_or_404()
    db.session.delete(data)
    db.session.commit()
    flash("Data berhasil di hapus.")
    return redirect(url_for("admin.view_category"))


@admin.route("/produk/lihat")
@login_required
def view_product():
    category = Category.query.count()
    data = (
        db.session.query(
            Product.slug,
            Product.id,
            Product.title,
            Product.description,
            Product.description_html,
            Product.image_filename,
            Product.doc_filename,
            Category.name.label("category"),
        )
        .join(Category, Category.id == Product.category_id)
        .all()
    )
    return render_template("view_product.html", data=data, category=category)


@admin.route("/produk/lihat/detail/<string:filename>", methods=["GET"])
@login_required
def view_document(filename):
    file_data = Product.query.filter_by(doc_filename=filename).first_or_404()
    return send_file(
        BytesIO(file_data.doc_data),
        attachment_filename=file_data.doc_filename,
        mimetype="application/pdf",
    )


@admin.route("/produk/tambah", methods=["GET", "POST"])
@login_required
def add_product():
    form = ProductForm()
    if form.validate_on_submit():
        data = Product(
            slug=slugify(form.title.data),
            title=form.title.data,
            description=form.description.data,
            category_id=form.category.data.id,
            image_filename=unique_name(form.photos.data),
            image_data=form.photos.data.read(),
        )
        if form.documents.data:
            data.doc_filename = unique_name(form.documents.data)
            data.doc_data = form.documents.data.read()
        db.session.add(data)
        db.session.commit()
        flash("Data berhasil di Tambah.")
        return redirect(url_for("admin.view_product"))
    return render_template("product.html", form=form)


@admin.route("/produk/edit/<slug>", methods=["GET", "POST"])
@login_required
def edit_product(slug):
    form = ProductForm()
    data = Product.query.filter_by(slug=slug).first_or_404()

    if form.validate_on_submit():
        data.slug = slugify(form.title.data)
        data.title = form.title.data
        data.description = form.description.data
        data.category_id = form.category.data.id
        data.image_filename = unique_name(form.photos.data)
        data.image_data = form.photos.data.read()
        if form.documents.data:
            data.doc_filename = unique_name(form.documents.data)
            data.doc_data = form.documents.data.read()
        db.session.commit()
        flash("Data berhasil di Update.")
        return redirect(url_for("admin.view_product"))
    form.title.data = data.title
    form.description.data = data.description
    form.category.data = data.category_id
    # form.photos.data = data.image_filename
    return render_template("product.html", form=form)


@admin.route("/produk/hapus/<slug>", methods=["GET", "POST"])
@login_required
def delete_product(slug):
    data = Product.query.filter_by(slug=slug).first_or_404()
    db.session.delete(data)
    db.session.commit()
    flash("Data berhasil di Hapus.")
    return redirect(url_for("admin.view_product"))

