from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms import (
    StringField,
    TextAreaField,
    SubmitField,
    PasswordField,
    BooleanField,
    ValidationError,
)
from wtforms.validators import DataRequired, Length, Email, Regexp, EqualTo
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from flask_pagedown.fields import PageDownField
from ..models import category_query, User, Category, Product


class UserForm(FlaskForm):
    name = StringField("Nama", validators=[DataRequired(), Length(3, 45)])
    username = StringField(
        "Username",
        validators=[
            DataRequired(),
            Length(3, 45),
            Regexp(r"^\w+$", message="Tidak boleh ada spasi dan spesial karakter."),
        ],
    )
    email = StringField("Email", validators=[DataRequired(), Email(), Length(1, 64)])
    password = PasswordField(
        "Password",
        validators=[
            DataRequired(),
            Length(6, 45),
            EqualTo("confirm", message="Password tidak cocok!"),
        ],
    )
    confirm = PasswordField(
        "Konfirmasi Password", validators=[DataRequired(), Length(6, 45)]
    )
    submit = SubmitField("Simpan")

    def validate_email(self, field):
        if User.query.filter_by(email=field.data.lower()).first():
            raise ValidationError("Email sudah terdaftar.")

    def validate_username(self, field):
        if User.query.filter_by(username=field.data.lower()).first():
            raise ValidationError("Username sudah digunakan.")


class ChangedPasswordForm(FlaskForm):
    old_password = PasswordField(u"Password Saat Ini", validators=[DataRequired()])
    password = PasswordField(
        u"Password Baru",
        validators=[
            DataRequired(),
            EqualTo("password2", message="Password harus sama"),
        ],
    )
    password2 = PasswordField(u"Konfirmasi Password", validators=[DataRequired()])
    submit = SubmitField("Update Password")


class PasswordResetRequestForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired(), Length(1, 64), Email()])
    submit = SubmitField("Reset Password")


class CategoryForm(FlaskForm):
    name = StringField(u"Nama Kategori", validators=[DataRequired(), Length(3, 50)])
    description = TextAreaField(
        u"Deskripsi", validators=[DataRequired(), Length(5, 255)]
    )
    photos = FileField(
        u"Gambar",
        validators=[
            FileRequired(u"Gambar tidak boleh kosong!"),
            FileAllowed(["jpeg", "jpg", "png"], u"Hanya upload file gambar!"),
        ],
    )
    submit = SubmitField(u"Simpan Kategori")

    def validate_name(self, field):
        if Category.query.filter_by(name=field.data.lower()).first():
            raise ValidationError("Nama kategori sudah ada.")


class ProductForm(FlaskForm):
    title = StringField(u"Nama Produk", validators=[DataRequired(), Length(3, 50)])
    description = PageDownField(
        u"Deskripsi", validators=[DataRequired(), Length(5, 255)]
    )
    photos = FileField(
        u"Gambar",
        validators=[
            FileRequired(u"Gambar tidak boleh kosong!"),
            FileAllowed(["jpeg", "jpg", "png"], u"Hanya upload file gambar!"),
        ],
    )
    documents = FileField(
        u"Dokumen", validators=[FileAllowed(["pdf"], u"Hanya upload file pdf!")]
    )
    category = QuerySelectField(
        u"Kategori Produk",
        validators=[DataRequired()],
        query_factory=category_query,
        allow_blank=True,
        get_label="name",
        get_pk=lambda x: x.id,
        blank_text=(u"Kategori"),
    )
    submit = SubmitField(u"Simpan Produk")

    def validate_title(self, field):
        if Product.query.filter_by(title=field.data.lower()).first():
            raise ValidationError("Nama product sudah ada.")

