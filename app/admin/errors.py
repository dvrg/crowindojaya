from flask import render_template
from . import admin


@admin.errorhandler(404)
def page_not_found(e):
    return render_template("404.html"), 404


@admin.errorhandler(403)
def forbiden(e):
    return render_template("403.html"), 403


@admin.errorhandler(500)
def internal_server_erro(e):
    return render_template("500.html"), 500

