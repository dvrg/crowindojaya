import os
from flask import (
    render_template,
    session,
    redirect,
    url_for,
    flash,
    request,
    send_file,
    make_response,
    current_app,
)
from flask_mail import Message
from datetime import datetime
from sqlalchemy import or_
from io import BytesIO
from . import main
from .. import db, moment
from .forms import ContactForm
from ..models import Category, Contact, Product
from ..email import send_email
from config import Config
from datetime import timedelta


def product_data():
    return Category.query.limit(4).all()


@main.route("/")
def index():
    product = product_data()
    data = (
        db.session.query(
            Product.slug.label("product_slug"),
            Product.id,
            Product.title,
            Product.description,
            Product.description_html,
            Product.image_filename,
            Product.doc_filename,
            Category.name.label("category"),
            Category.slug.label("category_slug"),
        )
        .join(Category, Category.id == Product.category_id)
        .order_by(Product.create_on.desc())
        .limit(4)
    )
    return render_template("index.html", data=data, product=product)


@main.route("/tentang-kami")
def about():
    product = product_data()
    return render_template("about.html", product=product)


@main.route("/produk")
def category():
    data = Category.query.all()
    product = product_data()
    return render_template("products.html", data=data, product=product)


@main.route("/produk/<slug>", methods=["GET"])
def list_product(slug):
    category = db.session.query(Category.name, Category.description).filter(
        Category.slug == slug
    )
    data = (
        db.session.query(
            Product.slug.label("product_slug"),
            Product.id,
            Product.title,
            Product.description,
            Product.description_html,
            Product.image_filename,
            Product.doc_filename,
            Category.id,
            Category.name.label("category"),
            Category.description.label("category_desc"),
            Category.slug.label("category_slug"),
        )
        .join(Category, Category.id == Product.category_id)
        .filter(Category.slug == slug)
    )
    product = product_data()
    return render_template(
        "list_product.html", data=data, category=category, product=product
    )


@main.route("/produk/<slug1>/<slug2>", methods=["GET"])
def detail_product(slug1, slug2):
    file_data = (
        Product.query.join(Category)
        .filter(Category.slug == slug1, Product.slug == slug2)
        .first_or_404()
    )
    return send_file(
        BytesIO(file_data.doc_data),
        attachment_filename=file_data.doc_filename,
        mimetype="application/pdf",
    )


@main.route("/kontak", methods=["GET", "POST"])
def contact():
    product = product_data()
    form = ContactForm()
    if form.validate_on_submit():
        contact = Contact.query.filter(
            or_(Contact.email == form.email.data, Contact.telepon == form.telp.data)
        ).first()
        if contact is None:
            data = Contact(
                name=form.name.data,
                company=form.company.data,
                telepon=form.telp.data,
                email=form.email.data,
                message=form.message.data,
                datetime=datetime.now(),
            )
            db.session.add(data)
            db.session.commit()

            subject = "Pesan Dari Kontak Form [crowindojaya.com]"
            html = render_template(
                "email.html",
                email=data.email,
                tel=data.telepon,
                messages=data.message,
                create_on=data.create_on,
            )
            send_email(subject, html, data.email)

            flash("Pesan berhasil tersimpan. Kami akan segera menghubungi anda.")
            return redirect(url_for("main.contact"))
        else:
            flash("Email atau Nomor Telepon sudah terdaftar!")
    return render_template("contact.html", form=form, product=product)


@main.route("/robots.txt", methods=["GET"])
def robots():
    return "User-agent: *\nDisallow: /admin"


@main.route("/sitemap.xml", methods=["GET"])
def sitemap():
    try:
        """Generate sitemap.xml. Makes a list of urls and date modified."""
        pages = []
        ten_days_ago = (datetime.now() - timedelta(days=7)).date().isoformat()
        # static pages
        for rule in current_app.url_map.iter_rules():
            if "GET" in rule.methods and len(rule.arguments) == 0:
                pages.append(
                    ["https://www.crowindojaya.com" + str(rule.rule), ten_days_ago]
                )

        sitemap_xml = render_template("sitemap_template.xml", pages=pages)
        response = make_response(sitemap_xml)
        response.headers["Content-Type"] = "application/xml"

        return response
    except Exception as e:
        return str(e)

