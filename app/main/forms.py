from flask_wtf import FlaskForm, RecaptchaField
from wtforms import StringField, TextAreaField, SubmitField
from wtforms.validators import DataRequired, Length, Email

class ContactForm(FlaskForm):
    name = StringField(u'Nama (*)', validators=[DataRequired(), Length(min=3, max=25)])
    company = StringField(u'Perusahaan')
    telp = StringField(u'Telepon (*)', validators=[DataRequired(), Length(min=10, max=16)])
    email = StringField(u'Email (*)', validators=[DataRequired(), Email()])
    message = TextAreaField(u'Pesan (*)', validators=[DataRequired(), Length(min=10, max=512)])
    submit = SubmitField(u'Kirim Pesan')
    recaptcha = RecaptchaField()