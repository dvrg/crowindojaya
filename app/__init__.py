from flask import (
    Flask,
    url_for,
    render_template,
    flash,
    session,
    redirect,
    request,
    send_file,
)
from flask_sqlalchemy import SQLAlchemy
from flask_moment import Moment
from flask_mail import Mail
from flask_login import LoginManager
from config import config
from flask_sslify import SSLify
from flask_pagedown import PageDown

db = SQLAlchemy()
moment = Moment()
mail = Mail()
pagedown = PageDown()
login_manager = LoginManager()
login_manager.login_view = "auth.login"
login_manager.session_protection = "strong"
login_manager.login_message = "Silahkan login terlebih dahulu."


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    db.init_app(app)
    moment.init_app(app)
    mail.init_app(app)
    pagedown.init_app(app)
    login_manager.init_app(app)

    if app.config["SSL_REDIRECT"]:
        sslify = SSLify(app)

    from .main import main as main_blueprint
    from .auth import auth as auth_blueprint
    from .admin import admin as admin_blueprint

    app.register_blueprint(main_blueprint)
    app.register_blueprint(auth_blueprint, url_prefix="/auth")
    app.register_blueprint(admin_blueprint, url_prefix="/admin")

    return app
