import hashlib
from markdown import markdown
import bleach
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app, request
from . import db, login_manager
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin, AnonymousUserMixin
from sqlalchemy.orm import validates

class Permissions:
    WRITE = 4
    ADMIN = 16

class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    default = db.Column(db.Boolean, default=False, index=True)
    permissions = db.Column(db.Integer)
    users = db.relationship('User', backref='role', lazy='dynamic')

    def __repr__(self):
        return '<User %r>' % self.name

    def __init__(self, **kwargs):
        super(Role, self).__init__(**kwargs)
        if self.permissions is None:
            self.permissions = 0

    def add_permission(self, perm):
        if not self.has_permission(perm):
            self.permissions += perm

    def remove_permission(self, perm):
        if self.has_permission(perm):
            self.permissions -= perm

    def reset_permission(self):
        self.permissions = 0

    def has_permission(self, perm):
        return self.permissions & perm == perm

    @staticmethod
    def insert_roles():
        roles = {
            'Moderator' : [Permissions.WRITE],
            'Administrator' : [Permissions.WRITE, Permissions.ADMIN]
        }

        default_role = 'Moderator'
        for r in roles:
            role = Role.query.filter_by(name=r).first()
            if role is None:
                role = Role(name=r)
            role.reset_permission()
            for perm in roles[r]:
                role.add_permission(perm)
            role.default = (role.name == default_role)
            db.session.add(role)
        db.session.commit()

class User(UserMixin, db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(45))
    email = db.Column(db.String(80), unique=True, index=True)
    username = db.Column(db.String(64), unique=True, index=True)
    password_hash = db.Column(db.String(128))
    create_on = db.Column(db.DateTime, nullable=True, default=datetime.now())
    last_seen = db.Column(db.DateTime, nullable=True, default=datetime.now())
    confirmed = db.Column(db.Boolean, default=False)
    avatar_hash = db.Column(db.String(32))
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))

    @validates('email, username')
    def lower(self, key, value):
        return value.lower()

    @property
    def password(self):
        raise AttributeError('Password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def ping(self):
        self.last_seen = datetime.utcnow()
        db.session.add(self)
        db.session.commit()

    def generate_confirmation_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'confirm': self.id}).decode('utf-8')
    
    def confirm(self, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token.encode('utf-8'))
        except:
            return False
        if data.get('confirm') != self.id:
            return False
        self.confirmed = True
        db.session.add(self)
        return True

    def generate_reset_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'reset': self.id}).decode('utf-8')

    @staticmethod
    def reset_password(token, new_password):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token.encode('utf-8'))
        except:
            return False
        user = User.query.get(data.get('reset'))
        if user is None:
            return False
        user.password = new_password
        db.session.add(user)
        return True

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        if self.role is None:
            if self.email == current_app.config['CROWINDO_ADMIN']:
                self.role = Role.query.filter_by(name='Administrator').first()
            if self.role is None:
                self.role = Role.query.filter_by(default=True).first()
        
        if self.email is not None and self.avatar_hash is None:
            self.avatar_hash = self.gravatar_hash()

    def can(self, perm):
        return self.role is not None and self.role.has_permission(perm)

    def is_administrator(self):
        return self.can(Permissions.ADMIN)

    def gravatar_hash(self):
        return hashlib.md5(self.email.lower().encode('utf-8')).hexdigest()

    def gravatar(self, size=100, default='identicon', rating='g'):
        if request.is_secure:
            url = 'https://secure.gravatar.com/avatar'
        else:
            url = 'https://www.gravatar.com/avatar'
        hash = self.avatar_hash or self.gravatar_hash()
        return '{url}/{hash}?s={size}&d={default}&r={rating}'.format(url=url, hash=hash, size=size, default=default, rating=rating)

def user_count():
    return User.query.count()

class AnonymousUser(AnonymousUserMixin):
    def can(self, permisions):
        return False

    def is_administrator(self):
        return False

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

class Category(db.Model):
    __tablename__ = 'category'
    __maxsize__ = 16777215 #4mb
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(45), unique=True, nullable=False)
    description = db.Column(db.String(255))
    image_filename = db.Column(db.String(255))
    image_data = db.Column(db.LargeBinary(__maxsize__))
    create_on = db.Column(db.DateTime, nullable=True, default=datetime.now())
    slug = db.Column(db.String(255), nullable=True, unique=True)
    
    @validates('name')
    def lower(self, key, value):
        return value.lower()

    def __init__(self, name, description, image_filename, image_data, slug):
        self.name = name
        self.description = description
        self.image_filename = image_filename
        self.image_data = image_data
        self.slug = slug

    def __repr__(self):
        return '<id: {}, name {}>'.format(self.id ,self.name)

def category_query():
    return Category.query

def category_count():
    return Category.query.count()

class Product(db.Model):
    __tablename__ = 'products'
    __maxsize__ = 4096 #4MB
    id = db.Column(db.Integer, primary_key=True)
    slug = db.Column(db.String(255), nullable=True, unique=True)
    title = db.Column(db.String(45), unique=True)
    description = db.Column(db.String(255))
    description_html = db.Column(db.Text)
    image_filename = db.Column(db.String(255))
    image_data = db.Column(db.LargeBinary(__maxsize__))
    doc_filename = db.Column(db.String(255))
    doc_data = db.Column(db.LargeBinary(__maxsize__))
    create_on = db.Column(db.DateTime, nullable=True, default=datetime.now())
    category_id = db.Column(db.ForeignKey(Category.id))
    products = db.relationship(Category, backref='products', lazy=True)

    @validates('title')
    def lower(self, key, value):
        return value.lower()

    def __init__(self, slug, title, description, category_id, image_filename, image_data):
        self.slug = slug
        self.title = title
        self.description = description
        self.category_id = category_id
        self.image_filename = image_filename
        self.image_data = image_data
        #self.doc_filename = doc_filename
        #self.doc_data = doc_data

    def __repr__(self):
        return '<title {}>'.format(self.title)

    @staticmethod
    def on_changed_body(target, value, oldvalue, initiator):
        allowed_tags = ['a', 'abbr', 'acronym', 'b', 'blockquote', 'code', 'em', 'i', 'li', 'ol', 'pre', 'strong', 'ul', 'h1', 'h2', 'h3', 'p']
        target.description_html = bleach.linkify(bleach.clean(markdown(value, output_format='html'), tags=allowed_tags, strip=True))

db.event.listen(Product.description, 'set', Product.on_changed_body)

def product_count():
    return Product.query.join(Category, Product.category_id == Category.id).count()

class Contact(db.Model):
    __tablename__ = 'contacts'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(45))
    company = db.Column(db.String(45))
    telepon = db.Column(db.String(45), unique=True)
    email = db.Column(db.String(80), unique=True)
    message = db.Column(db.String(255))
    create_on = db.Column(db.DateTime, nullable=True, default=datetime.now())

    @validates('email')
    def lower(self, key, value):
        return value.lower()

    def __init__(self, name, company, telepon, email, message, datetime):
        self.name = name
        self.company = company
        self.telepon = telepon
        self.email = email
        self.message = message
        self.datetime = datetime

    def __repr__(self):
        return '<id {}>'.format(self.id)

def contact_count():
    return Contact.query.count()

login_manager.anonymous_user = AnonymousUser