from flask_mail import Message
from threading import Thread
from flask import current_app
from . import mail

def send_async_email(app, msg):
    with app.app_context():
        mail.send(msg)

def send_email(subject, template, reply_to):
        app = current_app._get_current_object()
        msg = Message(subject, recipients=[current_app.config['CROWINDO_ADMIN'], current_app.config['MAIL_SECONDARY']], reply_to=reply_to, html=template)
        thr = Thread(target=send_async_email, args=[app, msg])
        thr.start()
        return thr

def confirm_email(subject, template, to):
        app = current_app._get_current_object()
        msg = Message(subject, recipients=[to], html=template)
        thr = Thread(target=send_async_email, args=[app, msg])
        thr.start()
        return thr