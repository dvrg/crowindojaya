from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms import (
    StringField,
    TextAreaField,
    SubmitField,
    PasswordField,
    BooleanField,
    ValidationError,
)
from wtforms.validators import DataRequired, Length, Email, Regexp, EqualTo
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from ..models import User


class LoginForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired(), Length(1, 64), Email()])
    password = PasswordField(u"Password", validators=[DataRequired(), Length(3, 25)])
    remember_me = BooleanField(u"Tetap Login")
    submit = SubmitField("Login")


class PasswordResetForm(FlaskForm):
    password = PasswordField(
        "Password Baru",
        validators=[
            DataRequired(),
            EqualTo("password2", message="Password harus cocok."),
        ],
    )
    password2 = PasswordField("Konfirmasi Password", validators=[DataRequired()])
    submit = SubmitField("Reset Password")

