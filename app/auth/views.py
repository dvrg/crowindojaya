from flask import url_for, render_template, request, redirect, flash, send_file
from flask_login import login_required, login_user, logout_user, current_user
from . import auth
from .forms import LoginForm, PasswordResetForm
from ..models import User
from ..email import confirm_email
from .. import db


@auth.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.ping()
        if (
            not current_user.confirmed
            and request.endpoint
            and request.blueprint != "auth"
            and request.endpoint != "static"
        ):
            return redirect(url_for("auth.unconfirmed"))


@auth.route("/unconfirmed")
def unconfirmed():
    if current_user.is_anonymous or current_user.confirmed:
        return redirect(url_for("admin.index"))
    return render_template("unconfirmed.html")


@auth.route("/confirm")
@login_required
def resend_confirmation():
    token = current_user.generate_confirmation_token()
    html = render_template("admin/mail/confirm.html", data=current_user, token=token)
    confirm_email("Konfirmasi Email", html, current_user.email)
    flash("Periksa email untuk konfirmasi.")
    return redirect(url_for("auth.login"))


@auth.route("/reset-password/<token>", methods=["GET", "POST"])
def password_reset(token):
    if not current_user.is_anonymous:
        return redirect(url_for("admin.index"))
    form = PasswordResetForm()
    if form.validate_on_submit():
        if User.reset_password(token, form.password.data):
            db.session.commit()
            flash("Password berhasil diganti. Silahkan login kembali.")
            return redirect(url_for("auth.login"))
        else:
            flash("Link reset password salah atau sudah tidak berlaku.")
            return redirect(url_for("admin.index"))
    return render_template("reset_password.html", form=form)


@auth.route("/konfirmasi/<token>")
@login_required
def confirm(token):
    if current_user.confirmed:
        return redirect(url_for("admin.index"))
    if current_user.confirm(token):
        db.session.commit()
        flash("Email Berhasil Terkonfirmasi!")
    else:
        flash("Link konfirmasi salah atau sudah tidak berlaku.")
    return redirect(url_for("admin.index"))


@auth.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is not None and user.verify_password(form.password.data):
            login_user(user, form.remember_me.data)
            next = request.args.get("next")
            if next is None or not next.startswith("/"):
                next = url_for("admin.index")
                flash("Berhasil Login.")
            return redirect(next)
        flash("Email atau Password Salah.")
    return render_template("login.html", form=form)


@auth.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Berhasil logout.")
    return redirect(url_for("auth.login"))
